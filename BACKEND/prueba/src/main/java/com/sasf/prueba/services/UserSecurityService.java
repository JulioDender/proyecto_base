package com.sasf.prueba.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sasf.prueba.model.Rol;
import com.sasf.prueba.model.Usuario;
import com.sasf.prueba.repository.UsuarioRepository;

@Service
public class UserSecurityService implements UserDetailsService{

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // TODO Auto-generated method stub
        Usuario usuario = this.usuarioRepository.findById(username)
            .orElseThrow(() -> new UsernameNotFoundException("Usuario "+username+ " no encontrado" ));

          String[] roles = usuario.getRoles().stream().map( r -> r.getId().getRol()).toArray(String[]::new) ;
            return User.builder()
            .username(usuario.getUsername())
            .password(usuario.getContraseña())
            .roles(roles)
            .accountLocked(usuario.getBloqueado())
            .disabled(usuario.getDeshabilitado())
            .build();
    
}

}
