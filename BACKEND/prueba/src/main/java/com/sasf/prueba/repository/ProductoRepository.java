package com.sasf.prueba.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sasf.prueba.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto,Long>{

    @Query(value="SELECT * FROM productos c WHERE c.categoriaLIKE :categoria", nativeQuery = true)
    List<Producto> buscarPorCategoria(@Param("categoria") String categoria);
}
