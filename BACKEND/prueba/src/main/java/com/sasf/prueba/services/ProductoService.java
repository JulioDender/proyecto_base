package com.sasf.prueba.services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.sasf.prueba.model.Producto;
import com.sasf.prueba.repository.ProductoRepository;


@Service
public class ProductoService {
    
    @Autowired
	private ProductoRepository productoRepository;

	
	 public List<Producto> obtenerProductos() {
		List<Producto> productos = productoRepository.findAll();
        return productos;
	}
    
	

	public Producto getProductoPorId(Long id) {
        
		Optional<Producto> dto = productoRepository.findById(id);
        return dto.get();
	}
		
	public boolean existeProducto(Long id) {	
		return productoRepository.existsById(id);
	}
	   
	
	//POST
	public Producto registrarProducto(Producto dto) {
		System.out.println(dto.toString());
		return productoRepository.save(dto);
	}
	
	//UPDATE
	public Producto actualizarDatos(Producto dto) {
		if(!this.existeProducto(dto.getId())) {
			throw new RuntimeException("El Producto ya se encuentra registrado.");
		}
		else{
			Producto ProductoModificado = productoRepository.findById(dto.getId()).get();
			if (dto.getNombre() != null) {
				ProductoModificado.setNombre(dto.getNombre());
			}
            if (dto.getDescripcion() != null) {
				ProductoModificado.setDescripcion(dto.getDescripcion());
			}
			if (dto.getCantidad() != null) {
				ProductoModificado.setCantidad(dto.getCantidad());
			}
			if (dto.getPrecio() != null) {
				ProductoModificado.setPrecio(dto.getPrecio());
			}
			if (dto.getMarca() != null) {
				ProductoModificado.setMarca(dto.getMarca());
			}
			if (dto.getCategoria() != null) {
				ProductoModificado.setCategoria(dto.getCategoria());
			}
			if (dto.getImagen() != null) {
				ProductoModificado.setImagen(dto.getImagen());
			}
			return productoRepository.save(ProductoModificado);
		}
		
	}
	
	//DELETE
	public	String eliminarProducto(Long id) {
		if (!this.existeProducto(id)) {
			throw new RuntimeException(
                    String.format(
                            "No existe el Producto con id: %s, por favor ingrese un id existente para eliminar.",
                            id));
		}
		else {
			productoRepository.deleteById(id);
			String mensaje ="{\"msg\":\"Producto eliminado correctamente\"}";
			return mensaje;
		}
		
	}
}
