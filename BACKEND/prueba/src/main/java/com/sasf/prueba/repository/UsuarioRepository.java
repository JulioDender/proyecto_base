package com.sasf.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasf.prueba.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario,String> {
    
}

