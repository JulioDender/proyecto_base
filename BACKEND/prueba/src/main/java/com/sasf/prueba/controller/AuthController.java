package com.sasf.prueba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sasf.prueba.model.Login;
import com.sasf.prueba.utils.JwtUtil;

@RestController
@RequestMapping("/auth")
public class AuthController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	
	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody Login loginEntity){
		UsernamePasswordAuthenticationToken login = new UsernamePasswordAuthenticationToken(loginEntity.getUsername(), loginEntity.getPassword());
		Authentication authentication = this.authenticationManager.authenticate(login);
		
		System.out.println(authentication.isAuthenticated());
		System.out.println(authentication.getPrincipal());
		
		String jwt = this.jwtUtil.create(loginEntity.getUsername());
		
		return ResponseEntity.ok().header(HttpHeaders.PRAGMA, jwt).build();	
				}
}
