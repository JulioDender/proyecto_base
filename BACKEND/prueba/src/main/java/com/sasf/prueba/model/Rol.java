package com.sasf.prueba.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
//@Table(name = "rol_usuario")
@Getter
@Setter
@NoArgsConstructor
public class Rol {

    @EmbeddedId
    private RolPK id;
    

    @Column(name="fecha_permiso")
    @NotNull
    private LocalDateTime fechaPermiso;

    @ManyToOne
   @JoinColumn(name ="usuario", referencedColumnName = "username", insertable = false, updatable = false)
   private Usuario user;
    
}

