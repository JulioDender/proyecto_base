package com.sasf.prueba.model;

import lombok.Data;

@Data
public class Login {
    private String username;
    private String password;
}
