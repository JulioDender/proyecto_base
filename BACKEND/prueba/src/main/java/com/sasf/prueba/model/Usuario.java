package com.sasf.prueba.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="usuario")
@Getter
@Setter
@NoArgsConstructor
public class Usuario {

    @Id
    @Valid
    @NotNull
    @Column(name="username")
    @Size(max=20)
    private String username;

    @NotNull
    @Size(max=200)
    private String contraseña;

    @Size(max=50)
    private String correo;

    @NotNull
    private Boolean bloqueado;

    @NotNull
  
    private Boolean deshabilitado;
    
   @OneToMany(mappedBy = "user",fetch = FetchType.EAGER)
    private List<Rol> roles;
}

