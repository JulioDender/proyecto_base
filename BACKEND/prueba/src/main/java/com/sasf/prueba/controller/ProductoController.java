package com.sasf.prueba.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sasf.prueba.model.Producto;
import com.sasf.prueba.services.ProductoService;


@RestController
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    //Consultar todos los usuarios
    @GetMapping("")
    public ResponseEntity<List<Producto>> getProductos() {
        return ResponseEntity.ok(productoService.obtenerProductos());
    }


    //Consultar usuario por ID
    @GetMapping("{id}")
    public ResponseEntity<Producto> getUsuario(@PathVariable Long id) {
        return ResponseEntity.ok(productoService.getProductoPorId(id));
    }

    //Guardar un usuario
    @Transactional
    @PostMapping("")
    public ResponseEntity<Producto> agregarUsuario(@RequestBody Producto dto){
        return ResponseEntity.ok(productoService.registrarProducto(dto));
    }

    @Transactional
    @PutMapping("/editar")
    public ResponseEntity<Producto> editarUsuario(@RequestBody Producto dto){
        return ResponseEntity.ok(productoService.actualizarDatos(dto));
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar(@PathVariable Long id) {
        return ResponseEntity.ok(productoService.eliminarProducto(id));
    }
}

