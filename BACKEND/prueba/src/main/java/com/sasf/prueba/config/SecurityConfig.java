package com.sasf.prueba.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class SecurityConfig {

    @Autowired
    private JwtFilter jwtFilter;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        
        http
            .csrf().disable()
    		.cors().and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
    		.authorizeHttpRequests()
            //.regexMatchers("/productos/*").permitAll()
            .regexMatchers("/auth/login").permitAll()
            /* .regexMatchers(HttpMethod.GET,"/clientes/todos").hasAnyRole("ADMIN","CUSTOMER")
            .regexMatchers(HttpMethod.POST,"clientes/agregar").hasRole("ADMIN")
            .regexMatchers(HttpMethod.PUT).hasRole("ADMIN") */
    		.anyRequest()
    		.authenticated()
    		.and()
    		.addFilterBefore(jwtFilter,BasicAuthenticationFilter.class);
		return http.build(); 
    }

    @Bean
	public AuthenticationManager authenticationManger(AuthenticationConfiguration configuration) throws Exception {
		return configuration.getAuthenticationManager();
	} 


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
}
