package com.sasf.prueba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="productos")
public class Producto {
    @Id
    @Valid
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idServicio;

    @NotNull
    private String nombre;

    @NotNull
    private String descripcion;

    @NotNull
    private Integer cantidad;

    @NotNull
    private Double precio;

    @NotNull
    private String marca;

    @NotNull
    private String categoria;

    @NotNull
    private String imagen;
}
