import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ProductosRoutingModule } from './productos-routing.module';
import { TableDataComponent } from './components/table-data/table-data.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { EditarComponent } from './components/editar/editar.component';
import { TablaBaseComponent } from './components/tabla-base/tabla-base.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TableDataComponent,
    TablaBaseComponent,
    FormularioComponent,
    EditarComponent,
  ],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    ReactiveFormsModule

  ]
})
export class ProductosModule { }
