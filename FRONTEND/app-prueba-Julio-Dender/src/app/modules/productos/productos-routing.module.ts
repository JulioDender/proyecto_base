import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableDataComponent } from './components/table-data/table-data.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { EditarComponent} from './components/editar/editar.component';
import { TablaBaseComponent } from './components/tabla-base/tabla-base.component';

const routes: Routes = [
  {path: '' , children:[
    { path: 'listarProducto_api', component: TableDataComponent },
    { path: 'crearProducto', component: FormularioComponent },
    { path: 'editarProducto', component: EditarComponent },
    { path: 'listarProducto_base', component: TablaBaseComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
