import { Component, OnInit } from '@angular/core';
import { Producto, ProductoCreado } from 'src/app/model/productos.model';
import { ProductoSer } from 'src/app/model/productosServicio.model';
import { ProductoService } from 'src/app/services/producto.service';



@Component({
  selector: 'app-table-data',
  templateUrl: './table-data.component.html',
  styleUrls: ['./table-data.component.css']
})
export class TableDataComponent implements OnInit {

  constructor(private productsService: ProductoService) { }
  // products: any[] = [];
  products!: any [];
  productsdb!: any[];
  productoPost ?: Producto;

  producto: any;


  ngOnInit(): void {
    this.getProducts();
  }

  validar(productoId: number){
    return this.productsdb.find(p => p.idServicio == productoId )? false: true;
  }



  getProducts(){
    this.productsService.getProducts().subscribe((data : any) =>{
      this.products = data.products
      // this.compareProductLists();
      console.log(this.products)
    })

    this.productsService.getProductsBase().subscribe((data : any) =>{
      this.productsdb = data
      // this.compareProductLists();
      console.log(this.productsdb)
    })
  }



  importProduct(p:ProductoSer){
    const productoExistente: ProductoCreado = {
      idServicio: p.id,
      nombre: p.title,
      descripcion: p.description,
      cantidad: p.stock,
      precio: p.price,
      marca: p.brand,
      categoria: p.category,
      imagen: p.thumbnail,
    };

    this.productsService.postProductsBase(productoExistente).subscribe((data : Producto) =>{
      console.log(this.products);
      this.getProducts();
    })
  }

}
