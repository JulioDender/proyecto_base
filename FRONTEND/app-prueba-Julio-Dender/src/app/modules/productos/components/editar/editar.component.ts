import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Producto, ProductoCreado } from 'src/app/model/productos.model';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent {

  producto!: ProductoCreado ;
  public productForm: FormGroup;

  constructor( 
    private fb: FormBuilder,
    private productoService: ProductoService,
    private router: Router ) {
      this.producto = this.productoService.getProductoActivo();
      this.productForm= this.fb.group({
        nombre: [this.producto.nombre ? this.producto.nombre : ''],
        descripcion: [this.producto.descripcion ? this.producto.descripcion:''],
        cantidad: [this.producto.cantidad ? this.producto.cantidad :''],
        precio: [ this.producto.precio ? this.producto.precio :''],
        marca: [  this.producto.marca ? this.producto.marca:''],
        categoria: [ this.producto.categoria ?this.producto.categoria : ''],
        imagen: [ this.producto.imagen ? this.producto.imagen : ''],
      })

    }


  logForm() {
    console.log(this.productForm.value);
  }

  ngOnInit() {
    console.log('a');
    
  }
  ngOnDestroy() {
    this.productForm.reset();
  }


 

   onSubmit() {
       if (this.productForm.valid) {

          this.productoService.actualizar(this.productForm.value).subscribe({
           next: (response: Producto) => {
             console.log('Dato actualizado correctamente');
             console.log(response);
             this.router.navigate(['/productos', 'listarProducto_base']); 
           },
           error: (error) => {
             console.log('Error en el  metodo updateUser');
             console.log(error);
           },
           complete: () => {
             console.log('Finalizacion del metodo updateUser');
           },
         });
       } else {
         console.log('Formulario no válido');
       } 
   }
}
