import { Component } from '@angular/core';
import { Producto } from 'src/app/model/productos.model';
import { ProductoService } from 'src/app/services/producto.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tabla-base',
  templateUrl: './tabla-base.component.html',
  styleUrls: ['./tabla-base.component.scss']
})
export class TablaBaseComponent {

  products!: Producto [];
  producto!: Producto;
  constructor(private productsService: ProductoService,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarProductos();
  }

  setProducto(producto: Producto){
    this.producto = producto;
    console.log(this.producto)
  }

  editUsuario(producto: Producto){
    this.productsService.setProducto(producto);
    this.router.navigate(['/productos', 'editarProducto']);
  }

  

  cargarProductos (): void {
    this.productsService.getProductsBase().subscribe((data : Producto[]) =>{
      this.products = data
      console.log(this.products)
    })
  }

  eliminar(producto:Producto): void {
    this.productsService.eliminar(producto.id).subscribe({
      next: (response)=>{
        console.log("Usuario eliminado")
        this.cargarProductos();
      },
      error: (error) =>{
        console.log("Error en el  metodo deleteUser")

      },
      complete: () =>{
        console.log("Finalizacion del metodo deleteUser")
      }
    })
  }



}
