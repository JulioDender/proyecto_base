import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaBaseComponent } from './tabla-base.component';

describe('TablaBaseComponent', () => {
  let component: TablaBaseComponent;
  let fixture: ComponentFixture<TablaBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TablaBaseComponent]
    });
    fixture = TestBed.createComponent(TablaBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
