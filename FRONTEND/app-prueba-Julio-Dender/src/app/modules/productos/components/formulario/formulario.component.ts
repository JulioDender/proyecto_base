import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Producto } from 'src/app/model/productos.model';
import { ProductoService } from 'src/app/services/producto.service';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent {
  

  public productForm: FormGroup = this.fb.group({
    nombre: [''],
    descripcion: [''],
    cantidad: [''],
    precio: [''],
    marca: [''],
    categoria: [''],
    imagen: [''],
  });

  constructor( 
    private fb: FormBuilder,
    private productoService: ProductoService,
    private router: Router ) {}


  logForm() {
    console.log(this.productForm.value);
  }

  ngOnInit() {
    console.log('a');
    
  }
  ngOnDestroy() {
    this.productForm.reset();
  }


 

   onSubmit() {
       if (this.productForm.valid) {

          this.productoService.crear(this.productForm.value).subscribe({
           next: (response: Producto) => {
             console.log('Dato actualizado correctamente');
             console.log(response);
             this.router.navigate(['/productos', 'listarProducto_base']); 
           },
           error: (error) => {
             console.log('Error en el  metodo updateUser');
             console.log(error);
           },
           complete: () => {
             console.log('Finalizacion del metodo updateUser');
           },
         });
       } else {
         console.log('Formulario no válido');
       } 
   }
}
