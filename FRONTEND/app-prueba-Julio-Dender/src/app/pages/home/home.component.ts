import { Component, Input, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/productos.model';
import { ProductoService } from 'src/app/services/producto.service'; 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products!: Producto[];
  @Input() datos: any;

  image?:string
  email?: string;
  avatar?: string
  firstName?: string
  lastName?:string
  age?:string
  phone?:string

  constructor(private productoService: ProductoService) { }

  ngOnInit(): void {
    this.email = sessionStorage.getItem("email")!
    this.firstName = sessionStorage.getItem("firstName")!
    this.lastName = sessionStorage.getItem("lastName")!
    this.image = sessionStorage.getItem("image")!
    this.phone = sessionStorage.getItem("phone")!
    this.age = sessionStorage.getItem("age")!

    this.getProductos()
  }
  
  getProductos(){
    this.productoService.getProductsBase().subscribe((products:Producto []) =>{
      this.products = products;
      this.products.reverse();
      console.log(products)
    })
  }

}
