import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { TokenService } from 'src/app/services/token.service';
import { Usuarios } from 'src/app/model/usuarios.model';
import { Login } from 'src/app/model/login.model';
import { HttpResponse } from '@angular/common/http';
import { Respuesta } from 'src/app/model/response.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin!: FormGroup
  users: Usuarios[] = []
  constructor(private  tokenService: TokenService, private loginService: LoginService, private formBuilder: FormBuilder, private router:Router) {

    this.formLogin = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })

   }

  ngOnInit(): void {
    this.loginService.getAllUsers().subscribe((users:any) =>{
      console.log(users)
      this.users = users.users
    })
  }

  login(){
    if(this.formLogin.valid){
      localStorage.removeItem('token');
      const value = this.formLogin.value
      console.log(this.users)
      const usuarioFind =  this.users.find((usuario) => usuario.username === value.username)
      const password = this.users.find((user => user.password === value.password))
      this.loginService.login(this.formLogin.value).subscribe({
        next: (response:any) => {
          console.log('Login completo');
          this.tokenService.saveToken(response.headers.get('pragma'));
          console.log(response);
          this.router.navigate(['/home'])
        },
        error: (error) => {
          console.log('Error en el  metodo updateUser');
          console.log(error);
        },
        complete: () => {
          console.log('Finalizacion del metodo updateUser');
        },
      });
      // var user = this.loginService.obtenerIdPorCorreo(value.email, this.users)
      // console.log(user)

      if(usuarioFind!=null && password!=null){
        sessionStorage.setItem("email", usuarioFind.email!)
        sessionStorage.setItem("firstName", usuarioFind.firstName!)
        sessionStorage.setItem("lastName", usuarioFind.lastName!)
        sessionStorage.setItem("phone", usuarioFind.phone!.toString());
        sessionStorage.setItem("age", usuarioFind.age!.toString());
        sessionStorage.setItem("image", usuarioFind.image!)

      }

    }
  }

}
