import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() enviarDatos: EventEmitter<any> = new EventEmitter();

  email?: string;
  avatar?: string
  firstName?: string
  lastName?:string
  
  constructor() { }

  

  ngOnInit(): void {
    this.email = sessionStorage.getItem("email")!
    this.firstName = sessionStorage.getItem("firstName")!
    this.lastName = sessionStorage.getItem("lastName")!
    this.avatar = sessionStorage.getItem("image")!
    
  }

}
