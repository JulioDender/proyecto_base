import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app-prueba-Julio-Dender';

  ismenurequired = false
  
  constructor(private router: Router){
    
  }

  ngDoCheck(): void {
    let currentUrl = this.router.url
    if( currentUrl == '/'){
      this.ismenurequired = false
    }else{
      this.ismenurequired = true
    }  }
}
