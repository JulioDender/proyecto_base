export interface Usuarios{
    id?: number,
    edad?:number,
    firstName?: string,
    lastName?:string,
    username?: string,
    email?: string,
    password?: string,
    image?: string,
    phone?:number,
    age?:number
}
