
export interface ProductoSer {
    id: number;
    title: string ;
    description: string;
    stock: number;
    price: number;
    brand: string;
    category: string;
    thumbnail: string;

}