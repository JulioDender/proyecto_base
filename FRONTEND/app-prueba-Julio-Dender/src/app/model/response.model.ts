import { HttpHeaders } from '@angular/common/http';
export interface Respuesta {
  body: any; // Tipo del cuerpo de la respuesta
  headers: {
    Authorization: string; // Tipo del encabezado de autorización
    'cache-Control': string; // Tipo del encabezado de autorización
    [key: string]: string; // Otros encabezados opcionales
  };
}
