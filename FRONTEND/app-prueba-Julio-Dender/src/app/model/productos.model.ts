
export interface Producto {
    id: number,
    idServicio?:number,
    nombre: string ,
    descripcion: string,
    cantidad: number,
    precio: number,
    marca: string,
    categoria: string,
    imagen: string,

}

export interface ProductoCreado extends Omit <Producto, 'id'>{

}

export interface ProductoParcial extends Partial<ProductoCreado>{

}