import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';
import { Usuarios } from '../model/usuarios.model';
import { Login } from '../model/login.model';
import { Observable, switchMap, tap } from 'rxjs';
import { TokenService } from './token.service';
import { Respuesta } from '../model/response.model';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  urlBase: string = `${environment.apiUrl}/auth/login`;


  constructor(private http: HttpClient,
              private tokenService: TokenService ) { }

  login(login : any){
    const url = this.urlBase;
    return this.http.post<Respuesta>(url, login,{ observe: 'response' });
  }

  getAllUsers(){
    return this.http.get<Usuarios[]>(environment.base_url_login)
   }

    obtenerIdPorCorreo(email: string, user: Usuarios[]){
      const usuarioFind =  user.find((usuario) => usuario.email === email)

      return usuarioFind
    }
}
