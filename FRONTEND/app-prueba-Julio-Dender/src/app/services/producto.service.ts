import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';
import { ProductoSer } from '../model/productosServicio.model';
import { Producto } from '../model/productos.model';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  urlServicio: string = `${environment.servicio}`;
  urlBase: string = `${environment.apiUrl}/productos`;
  producto!: Producto;

  constructor(private http: HttpClient) { }



  getProducts(){
    return this.http.get<any[]>(environment.servicio)
    
  }

  getProductsBase(){
    return this.http.get<any[]>(this.urlBase)
    
  }

  setProducto( u:any){
    this.producto= u;
  }

  getProductoActivo(): any {
    return this.producto
  }

  postProductsBase(producto:any): Observable<any> {
    const url = `${environment.apiUrl}`;
    return this.http.post(this.urlBase, producto);
  
  }

  crear(producto : any){
    const url = this.urlBase;
    return this.http.post<Producto>(url, producto);
  }

  actualizar(producto : any): Observable<any>{
    producto.id = this.producto.id;
    const url = `${this.urlBase}/editar`;
    return this.http.put<Producto>(url, producto);
  }

  eliminar(id: number){
    const url = `${this.urlBase}/eliminar/${id}`;
    return this.http.delete<String>(`${this.urlBase}/eliminar/${id}`);
  }
}
